package com.gitlab.sm1le.fastresp;

import com.gitlab.sm1le.events.LiteEvent;
import org.bukkit.Bukkit;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class FastRespawnPlugin extends JavaPlugin {

    @Override
    public void onEnable() {
        new LiteEvent()
                .event(PlayerDeathEvent.class, event -> {
                    Bukkit.getScheduler().runTaskLater(this, () -> event.getEntity().spigot().respawn(), 5);
                }).register(this);
    }
}
